provider "nutanix" {
    username     = var.username
    password     = var.password
    endpoint     = var.endpoint
    insecure     = var.insecure
    port         = var.port
    wait_timeout = var.wait_timeout
}