#mandatory parameters
username           = "myuser"
password           = "mypassword"
endpoint           = "10.10.10.10" # IP of Prism Element or Prism Central
automation_ssh_key = "mypublickey" # can be found by running cat ~/.ssh/id_rsa.pub

#optional parameters
insecure     = true
port         = 9440
wait_timeout = 10